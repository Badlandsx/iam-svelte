Svelte getting started
=========

What is svelte
------

- Svelte provides a way to build declarative, state driven user interfaces
- Unlike other JS frameworks, Svelete is a compiler. It does not run in the browser. Your compiled code will directly modify the DOM without the overhead of the "virtual DOM"
- Svelte produces the code that will manipulate the DOM at build time. It's not something that you need to load on the client side (browser)

VScode extensions
------

- Svelte for VS Code
- Eslint
- Prettier - Code Formatter

Svelte help
------

- [Tools sveltesociety](https://sveltesociety.dev/packages)
- [Playground REPL](https://svelte.dev/repl/hello-world?version=4.2.9)

Svelte vs SvelteKit
------

Svelte:

- Component based Javascript Framework
- Allows you to build state drivven components for the web in a declarative way

SvelteKit:

- Framework to build extremely high-performance web apps
- Routing, prefetching, offline, support, SSR, progressive enhancement and more

> SvelteKit to Svelte is like NextJS to ReactJS

Getting started simple project
------

> [Introduction](https://svelte.dev/docs/introduction)

``
npm create svelte@latest svelte1
cd svelte1
npm install
npm run dev
``
